from django.contrib.auth.decorators import login_required as auth
from django.conf.urls import include, url
from django.contrib import admin
from links.views import UserProfileDetailView, UserProfileEditView
from links.views import LinkCreateView, LinkUpdateView, LinkDeleteView, LinkDetailView
from links.views import VoteFormView
from links.views import TopicListView, TrendingAll, NewAll, TopicView, NewsTrending

from registration.backends.default.views import RegistrationView
from registration.forms import RegistrationFormUniqueEmail
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import password_change

class RegistrationViewUniqueEmail(RegistrationView):
    form_class = RegistrationFormUniqueEmail


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TrendingAll.as_view(template_name="links/all_trending.html"), name='home'),
    url(r'^(?i)topic/all/$', TrendingAll.as_view(template_name="links/all_trending.html"), name='home'),
    url(r'^(?i)topic/all/trending$', TrendingAll.as_view(template_name="links/all_trending.html"), name='home'),
    url(r'^(?i)topic/all/new/$', NewAll.as_view(template_name="links/all_new.html"), name="new"),
    url(r'^(?i)topic/news/trending$', NewsTrending.as_view(template_name="links/news_trending.html"), name="news_trending"),
    url(r'^(?i)topic/news/$', NewsTrending.as_view(template_name="links/news_trending.html"), name="news_trending"),
    url(r'^(?i)topic/news/new$', NewsTrending.as_view(template_name="links/news_new.html"), name="news_new"),
    url(r'^login/$', 'django.contrib.auth.views.login', {
            'template_name': 'login.html'}, name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name="logout"),
    # url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    # url(r'^accounts/', include('captcha.backends.hmac.urls')),
    url(r'^users/(?P<slug>\w+)/$', UserProfileDetailView.as_view(), name="profile"),
    url(r'^user/register', RegistrationViewUniqueEmail.as_view(), name='registration_register'),
    url(r'^edit_profile/$', auth(UserProfileEditView.as_view()), name="edit_profile"),
    url(r'^link/create/$', auth(LinkCreateView.as_view()), name='link_create'),
    url(r'^link/(?P<pk>\d+)/$', LinkDetailView.as_view(), name='link_detail'),
    url(r'^(?i)topics/list/$', TopicListView.as_view(), name="topics"),
    url(r'^(?i)topics/$', TopicListView.as_view(), name="topics"),
    # url(r'^topic/(?P<topic>\w+)/$', TopicView.as_view(template_name="links/all_trending.html")),
    # url(r'^list/(?P<pk>\w+)/$', LinkListView.as_view(), name='link_list'),
    url(r'^link/update/(?P<pk>\d+)/$', auth(LinkUpdateView.as_view()), name='link_update'),
    url(r'^link/delete/(?P<pk>\d+)/$', auth(LinkDeleteView.as_view()), name='link_delete'),
    url(r'^comments/', include('django_comments_xtd.urls')),
    url(r'^vote/$', auth(VoteFormView.as_view()), name="vote"),
    url(r'^/accounts/password/change/$', password_change, {'template_name': 'registration/password_reset_form.html'}),

    #     url(r'^password/change/$',
    #     auth_views.password_change,
    #     {'post_change_redirect': 'auth_password_change_done'},
    #     name='auth_password_change'),
    # url(r'^password/change/done/$',
    #     auth_views.password_change_done,
    #     name='auth_password_change_done'),
        # Change Password URLs:
    # url(r'^accounts/password_change/$', 'django.contrib.auth.views.password_change', {'post_change_redirect' : '/accounts/password_change/done/'}, name="password_change"),
    # url(r'^accounts/password_change/done/$', 'django.contrib.auth.views.password_change_done'),
]
