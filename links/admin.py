from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

from .models import Link, Topic, Vote, Badge
from .models import UserProfile


class TopicAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumb_url', 'created_on', 'description', 'short_name')
admin.site.register(Topic, TopicAdmin)

class LinkAdmin(admin.ModelAdmin): pass
admin.site.register(Link, LinkAdmin)

class VoteAdmin(admin.ModelAdmin): pass
admin.site.register(Vote, VoteAdmin)

class BadgeAdmin(admin.ModelAdmin): pass
admin.site.register(Badge, BadgeAdmin)

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False


class UserProfileAdmin(UserAdmin):
    inlines = (UserProfileInline, )

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), UserProfileAdmin)
