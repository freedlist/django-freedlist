from django.db import models
from django.db.models import Count
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.utils.timezone import now
from bs4 import BeautifulSoup
import urllib3
import re


class LinkVoteCountManager(models.Manager):
    def get_query_set(self):
        return super(LinkVoteCountManager, self).get_query_set().annotate(
            votes=Count('vote')).order_by('-rank_score', '-votes')


class Topic(models.Model):
    title = models.CharField("Title", max_length=48)
    thumb_url = models.URLField("Thumbnail URL", max_length=2083, blank=True)
    created_on = models.DateTimeField(auto_now_add=True)
    description = models.TextField(blank=True)
    short_name = models.CharField("Short Name", max_length=48)
    topics = models.Manager()

    def __unicode__(self):
        return self.title


class Link(models.Model):
    title = models.CharField("Headline", max_length=250)
    submitter = models.ForeignKey(User)
    topic = models.ForeignKey(Topic, verbose_name="Topic name")
    submitted_on = models.DateTimeField(auto_now_add=True)
    rank_score = models.FloatField(default=0.0)
    url = models.URLField("URL", max_length=2083, blank=True)
    thumb_url = models.URLField("Thumbnail URL", max_length=2083, blank=True)
    description = models.TextField(blank=True)
    with_votes = LinkVoteCountManager()
    objects = models.Manager() #default manager
    needs_moderation = models.BooleanField(default=False)

    @property
    def votes(self):
        voted = Vote.votes.filter(link=self)
        return len(voted)


    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("link_detail", kwargs={"pk": str(self.id)})

    def set_rank(self):
        # Based on HN ranking algo at http://amix.dk/blog/post/19574
        # Based on HN ranking algo at http://amix.dk/blog/post/19574
        SECS_IN_HOUR = float(60*60)
        GRAVITY = 1.2

        delta = now() - self.submitted_on
        item_hour_age = delta.total_seconds() // SECS_IN_HOUR
        votes = self.votes - 1
        self.rank_score = votes / pow((item_hour_age+2), GRAVITY)
        super(Link, self).save()


    def save(self, *args, **kwargs):

        http = urllib3.PoolManager()
        page = http.request('GET', self.url)
        soup = BeautifulSoup(page.data, 'html.parser')
        meta = soup.findAll('meta')

        content = {}
        for tag in meta:
            if tag.has_key('property'):
                if re.search('og:', tag['property']) is not None:
                    content[re.sub('og:', '', tag['property'])] = tag['content']

        if "image" in content:
            self.thumb_url = content["image"]
        if "description" in content:
            self.description = content["description"]
        if "title" in content:
            self.title = content["title"]

        super(Link, self).save(*args, **kwargs)


class Badge(models.Model):
    title = models.CharField("Name", max_length=250)
    thumb_url = models.URLField("Thumbnail URL", max_length=2083, blank=True)
    badges = models.Manager()

    def __unicode__(self):
        return "%s Badge" % self.title


class Vote(models.Model):
    voter = models.ForeignKey(User)
    link = models.ForeignKey(Link)
    submitted_on = models.DateTimeField(auto_now_add=True)
    votes = models.Manager()

    def __unicode__(self):
        return "%s voted %s" % (self.voter.username, self.link.title)

class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True)
    bio = models.TextField(null=True, blank=True)
    twitter = models.CharField("twitter handle", max_length=250, blank=True)
    # email = models.EmailField()
    newsletter = models.BooleanField(default=False)
    subscriptions = models.ManyToManyField(Topic, blank=True)
    cookies = models.IntegerField(default=1)
    badge = models.ManyToManyField(Badge, blank=True)

    def __unicode__(self):
        return "%s's profile" % self.user

def create_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)

from django.db.models.signals import post_save
post_save.connect(create_profile, sender=User)
