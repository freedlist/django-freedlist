# from django.views.generic import ListView, DetailView
# from django.views.generic.edit import UpdateView, DeleteView, CreateView, FormView
# from django.contrib.auth import get_user_model
# from django.shortcuts import redirect
# from django.shortcuts import get_object_or_404
# from .models import Link, Vote, UserProfile, LinkList
# from .forms import UserProfileForm
# from .forms import LinkForm, VoteForm
# from django.core.urlresolvers import reverse, reverse_lazy
# #from django import template


import json
import operator
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.views.generic import ListView
from django.views.generic import DetailView
from .models import Link, Topic, Badge, Vote, UserProfile
from .forms import UserProfileForm, LinkForm, VoteForm

from django.contrib.auth import get_user_model
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse, reverse_lazy


class JSONFormMixin(object):
    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict), content_type='application/json')
        response.status = 200 if valid_form else 500
        return response

class VoteFormBaseView(FormView):
    form_class = VoteForm

    def create_response(self, vdict=dict(), valid_form=True):
        response = HttpResponse(json.dumps(vdict))
        response.status = 200 if valid_form else 500
        return response

    def form_valid(self, form):
        link = get_object_or_404(Link, pk=form.data["link"])
        user = self.request.user
        prev_votes = Vote.votes.filter(voter=user, link=link)
        has_voted = (len(prev_votes) > 0)

        ret = {"success": 1}
        if not has_voted:
            # add vote
            v = Vote.votes.create(voter=user, link=link)
            ret["voteobj"] = v.id
        else:
            # delete vote
            prev_votes[0].delete()
            ret["unvoted"] = 1
        return self.create_response(ret, True)


    def form_invalid(self, form):
        ret = {"success": 0, "form_errors": form.errors }
        return self.create_response(ret, False)


class VoteFormView(JSONFormMixin, VoteFormBaseView):
    pass

class UserProfileEditView(UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = "edit_profile.html"

    def get_object(self, queryset=None):
        return UserProfile.objects.get_or_create(user=self.request.user)[0]

    def get_success_url(self):
        return reverse("profile", kwargs={'slug': self.request.user})

    def get_context_data(self, **kwargs):
        context = super(UserProfileEditView, self).get_context_data(**kwargs)
        context["user"] = self.request.user
        return context


class TopicListView(ListView):
        model = Topic
        paginate_by = 50


class TrendingAll(ListView):
    model = Link
    queryset = Link.with_votes.order_by('-rank_score')
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(TrendingAll, self).get_context_data(**kwargs)
        context["topics"] = Topic.topics.all().order_by("title")
        if self.request.user.is_authenticated():
            voted = Vote.votes.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context

class NewAll(ListView):
    model = Link
    queryset = Link.with_votes.order_by('-submitted_on')
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(NewAll, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.votes.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context


class TopicView(ListView):
    model = Link
    queryset = Link.with_votes.order_by('-rank_score')
    queryset = queryset.filter(topic=Topic.topics.filter(title="Entertianment"))
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(TopicView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.votes.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context

class NewsTrending(ListView):
    model = Link
    queryset = Link.with_votes.order_by('-rank_score')
    queryset = queryset.filter(topic=Topic.topics.filter(title="News"))
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(NewsTrending, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.votes.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context


class NewsNew(ListView):
    model = Link
    queryset = Link.with_votes.order_by('-submitted_on')
    queryset = queryset.filter(topic=Topic.topics.filter(title="News"))
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(NewsNew, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            voted = Vote.votes.filter(voter=self.request.user)
            links_in_page = [link.id for link in context["object_list"]]
            voted = voted.filter(link_id__in=links_in_page)
            voted = voted.values_list('link_id', flat=True)
            context["voted"] = voted
        return context

class UserProfileDetailView(DetailView):
        model = get_user_model()
        slug_field = "username"
        template_name = "user_detail.html"

        def get_object(self, queryset=None):
            user = super(UserProfileDetailView, self).get_object(queryset)
            UserProfile.objects.get_or_create(user=user)
            return user


class LinkCreateView(CreateView):
    model = Link
    form_class = LinkForm

    def form_valid(self, form):
        f = form.save(commit=False)
        f.rank_score = 0.0
        f.submitter = self.request.user
        f.save()

        return super(CreateView, self).form_valid(form)


class LinkDetailView(DetailView):
    model = Link


class LinkUpdateView(UpdateView):
    model = Link
    form_class = LinkForm


class LinkDeleteView(DeleteView):
    model = Link
    success_url = reverse_lazy("home")
