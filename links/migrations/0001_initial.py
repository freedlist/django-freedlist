# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250, verbose_name=b'Name')),
                ('thumb_url', models.URLField(max_length=2083, verbose_name=b'Thumbnail URL', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250, verbose_name=b'Headline')),
                ('submitted_on', models.DateTimeField(auto_now_add=True)),
                ('rank_score', models.FloatField(default=0.0)),
                ('url', models.URLField(max_length=2083, verbose_name=b'URL', blank=True)),
                ('thumb_url', models.URLField(max_length=2083, verbose_name=b'Thumbnail URL', blank=True)),
                ('description', models.TextField(blank=True)),
                ('needs_moderation', models.BooleanField(default=False)),
                ('submitter', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=48, verbose_name=b'Title')),
                ('thumb_url', models.URLField(max_length=2083, verbose_name=b'Thumbnail URL', blank=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bio', models.TextField(null=True)),
                ('twitter', models.CharField(max_length=250, verbose_name=b'twitter handle')),
                ('email', models.EmailField(max_length=254)),
                ('newsletter', models.BooleanField(default=False)),
                ('cookies', models.IntegerField(default=1)),
                ('badge', models.ManyToManyField(to='links.Badge')),
                ('subscriptions', models.ManyToManyField(to='links.Topic')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('submitted_on', models.DateTimeField(auto_now_add=True)),
                ('link', models.ForeignKey(to='links.Link')),
                ('voter', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='link',
            name='topic',
            field=models.ForeignKey(verbose_name=b'Topic name', to='links.Topic'),
        ),
    ]
