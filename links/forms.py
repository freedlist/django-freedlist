from django import forms
from .models import UserProfile, Link, Vote

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ("user", "cookies", "badge")


class LinkForm(forms.ModelForm):
    class Meta:
        model = Link
        exclude = ("submitter", "rank_score", "needs_moderation", "title", "thumb_url", "description")


class VoteForm(forms.ModelForm):
    class Meta:
        model = Vote
        exclude = ("submitted_on", )
